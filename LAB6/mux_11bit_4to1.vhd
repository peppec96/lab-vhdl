LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY mux_11bit_4to1 IS
 PORT(  U,V, W, X: IN  SIGNED(10 DOWNTO 0);
S: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
 M    : OUT SIGNED(10 DOWNTO 0));
end mux_11bit_4to1;

ARCHITECTURE structure of mux_11bit_4to1 IS

COMPONENT mux4to1
 PORT(u,v,w,x: IN STD_LOGIC;
       S: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
       m: OUT STD_LOGIC);
end COMPONENT;

BEGIN
gen: for i in 0 to 10 generate
      Muxi: mux4to1 PORT MAP          
(U(10-i),V(10-i),W(10-i),X(10-i),S,M(10-i));
end generate;

end structure;