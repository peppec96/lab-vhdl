library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity memoryB is
  port(CLK: in std_logic;
   DATA_IN:in signed(7 downto 0);
   ADDRESS:in unsigned(9 downto 0); --pointer for the cell
	CS,WR_B:in std_logic;
   DATA_OUT:out signed(7 downto 0)
       );
end memoryB;


architecture Be of memoryB is
  subtype word is signed(7 downto 0);
  type memory is array(0 to 1023) of word;
  
begin
  main_p:process(CLK) is
  begin
  if rising_edge(CLK) then
  
  if CS='1' then
   if WR_B='1' then
	 memory(to_integer(ADDRESS))<=DATA_IN;
  end if;
  end if;
  end if;
      
  end process main_p;
  
end be;
