LIBRARY ieee; 
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all; 
 
ENTITY reg8 IS  
GENERIC ( N : integer:=8);  
PORT (R   : IN  SIGNED(N-1 DOWNTO 0); 
RESET: IN STD_LOGIC;
LOAD: IN STD_LOGIC;  
Clock: IN  STD_LOGIC;   
Q   : OUT SIGNED(N-1 DOWNTO 0)); 
END reg8; 
 
ARCHITECTURE Behavior OF reg8 IS 
BEGIN  
PROCESS (Clock)  
BEGIN  

IF RESET ='0' THEN
 Q<="00000000";
ELSIF (Clock'EVENT AND Clock = '1') THEN 
IF LOAD ='1'  then
Q <= R;  
 END IF;  
END IF;
END PROCESS; 
END Behavior;