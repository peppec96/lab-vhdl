LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux4to1 IS
 PORT(u,v,w,x: IN STD_LOGIC;
       S: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
       m: OUT STD_LOGIC);
end mux4to1;

ARCHITECTURE structure of mux4to1 IS
--SIGNAL f1,f2,f3: STD_lOGIC;

BEGIN
    WITH S SELECT
      M <= U when "00" ,
           V when "01" ,
           W when "10" ,
           X when OTHERS ;
end structure;