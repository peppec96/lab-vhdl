LIBRARY ieee; 
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all; 
 
ENTITY regn IS  
GENERIC ( N : integer:=7);  
PORT (R   : IN  STD_LOGIC_VECTOR(N-1 DOWNTO 0);   
Clock: IN  STD_LOGIC;   
Q   : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0)); 
END regn; 
 
ARCHITECTURE Behavior OF regn IS 
BEGIN  
PROCESS (Clock)  
BEGIN  
IF (Clock'EVENT AND Clock = '1') THEN    
Q <= R;   END IF;  
END PROCESS; 
END Behavior;