library ieee;
USE ieee.std_logic_1164.all;

ENTITY mux2to1 IS
PORT(
X,Y: IN STD_LOGIC_VECTOR(8 DOWNTO 0);
sel: IN STD_LOGIC;
Z:OUT STD_LOGIC_VECTOR(8 DOWNTO 0));
END mux2to1;

ARCHITECTURE structure OF mux2to1 IS
BEGIN
with sel select
Z<=X when '0',
   Y when others;
  
END structure;