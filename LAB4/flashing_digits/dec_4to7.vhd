library ieee;
USE ieee.std_logic_1164.all;

ENTITY dec_4to7 IS
PORT (X: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      Y: OUT STD_LOGIC_VECTOR(0 TO 6));
end dec_4to7;

ARCHITECTURE behavior OF dec_4to7 IS

begin
with X SELECT
Y<= "0000001" when "0000",  --0
 "1001111" when "0001",  --1
 "0010010" when "0010",  --2
 "0000110" when "0011",  --3
 "1001100" when "0100",  --4
 "0100100" when "0101",  --5
 "0100000" when "0110",  --6
 "0001111" when "0111",  --7
 "0000000" when "1000",  --8
 "0000100" when "1001",  --9
 "0000001" when others;  --10

END Behavior;
