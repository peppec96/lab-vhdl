library ieee;
USE ieee.std_logic_1164.all;

ENTITY counter_4bit IS
PORT(EN, CLOCK, Clear: IN STD_LOGIC;
     Q: OUT STD_LOGIC;
	  Q_OUT : BUFFER STD_LOGIC_VECTOR (3 DOWNTO 0)
);
END counter_4bit;

ARCHITECTURE Behavior OF counter_4bit IS

SIGNAL T_INT : STD_LOGIC_VECTOR (3 DOWNTO 1);

COMPONENT T_flipflop IS
PORT(T, CLK, Clear: IN STD_LOGIC;
     Q: OUT STD_LOGIC);
END COMPONENT;
BEGIN 
T_INT(3)<=EN AND Q_OUT(3);
T_INT(2)<=T_INT(3) AND Q_OUT(2);
T_INT(1)<=T_INT(2) AND Q_OUT(1);
Q<=T_INT(1) AND Q_OUT(0);


tff1: T_flipflop PORT MAP
(EN,CLOCK,Clear,Q_OUT(3));
tff2: T_flipflop PORT MAP
(T_INT(3),CLOCK,Clear,Q_OUT(2));
tff3: T_flipflop PORT MAP
(T_INT(2),CLOCK,Clear,Q_OUT(1));
tff4: T_flipflop PORT MAP
(T_INT(1),CLOCK,Clear,Q_OUT(0));

END Behavior;


