library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY NEURAL_NETWORK IS 
PORT( DATA_IN: IN SIGNED(7 DOWNTO 0);
CLOCK: IN STD_LOGIC;
START: IN STD_LOGIC;
DONE: OUT STD_LOGIC;
POS_VALUES: IN UNSIGNED(10 DOWNTO 0)
);
end NEURAL_NETWORK;

ARCHITECTURE STRUCTURE OF NEURAL_NETWORK IS
 COMPONENT DATA_PATH 
PORT(D_IN: IN SIGNED (7 DOWNTO 0);
    CNT_RESET: IN STD_LOGIC;
	 C11_RST: IN STD_LOGIC;
    L1,L2,L3: IN STD_LOGIC;
    CLOCK: IN STD_LOGIC;
    SEL: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
	 SEL2: IN STD_LOGIC;
    POS_VALUES: OUT UNSIGNED(10 DOWNTO 0);
	 DATA_B: OUT SIGNED( 7 DOWNTO 0);
	 ADDRESS: OUT STD_LOGIC_VECTOR (0 TO 9)
	 ); END COMPONENT;
 
 --COMPONENT CONTROL_UNIT
-- END COMPONENT;
 
 COMPONENT memoryA 
 port(CLK: in std_logic;
   DATA_IN:in signed(7 downto 0);
   ADDRESS:in unsigned(9 downto 0); --pointer for the cell
   CS,WR,RD: in std_logic;
      DATA_OUT:out signed(7 downto 0)
       );
 END COMPONENT;
 
 COMPONENT memoryB
 port(CLK: in std_logic;
   DATA_IN:in signed(7 downto 0);
   ADDRESS:in unsigned(9 downto 0); --pointer for the cell
	CS,WR_B:in std_logic;
   DATA_OUT:out signed(7 downto 0)
       );
 END COMPONENT;
 
 BEGIN 
 
END STRUCTURE;