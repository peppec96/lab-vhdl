library ieee;
USE ieee.std_logic_1164.all;

ENTITY one_hot_FSM_with_process IS
PORT( KEY:IN STD_LOGIC_VECTOR (1 DOWNTO 0);
 SW:IN STD_LOGIC_VECTOR (1 DOWNTO 0);
 LEDG:OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
 LEDR:OUT STD_LOGIC_VECTOR (8 DOWNTO 0));
END one_hot_FSM_with_process;

ARCHITECTURE BEHAVIOR OF one_hot_FSM_with_process IS
SIGNAL  CLK,RESET,w: STD_LOGIC;
SIGNAL z: STD_LOGIC;
TYPE State_type IS (A, B, C, D, E, F, G, H, I);  
SIGNAL y_Q, Y_D : State_type;  -- y_Q is present state, y_D is next state
SIGNAL PS: STD_LOGIC_VECTOR(8 DOWNTO 0);
 BEGIN 
 
CLK<=KEY(0);
RESET<=SW(0);
w<=SW(1);

State_transition: PROCESS (w,y_Q,Y_D) -- state table 
 BEGIN 
  CASE y_Q IS 
  
WHEN A=> IF (w = '0')  THEN Y_D <= B; 
ELSE Y_D <= F;  
 END IF; 

WHEN B=> IF (w = '0') THEN Y_D <= C;    
  ELSE Y_D <= F;    
 END IF;
 
WHEN C=> IF (w = '0')  THEN Y_D <= D;    
  ELSE Y_D <= F;    
 END IF;
 
WHEN D=> IF ( w = '0')  THEN Y_D <= E;    
  ELSE Y_D <= F;    
 END IF;
 
WHEN E=> IF (w = '0')  THEN Y_D <= E;   
  ELSE Y_D <= F;    
 END IF;
 
WHEN F=> IF (w = '0') THEN Y_D <= B;    
  ELSE Y_D <= G;    
 END IF;
 
WHEN G=> IF (w = '0') THEN Y_D <= B;    
  ELSE Y_D <= H;    
 END IF;
 
WHEN H=> IF (w = '0') THEN Y_D <= B;    
  ELSE Y_D <= I;    
 END IF;
 
WHEN I=> IF (w = '0') THEN Y_D <= B;    
  ELSE Y_D <= I;    
 END IF;
 
 END CASE; 
 END PROCESS State_transition; -- state table 
 
Register_process: PROCESS (CLK,RESET) -- state flip-flops 
BEGIN 
IF RESET = '0' THEN     
 Y_Q <= A;       
 ELSIF( rising_edge(CLK) ) THEN  
 Y_Q <= y_D;       
 END IF; 
END PROCESS Register_process;

Output_decode: PROCESS(z,Y_Q)
BEGIN

CASE Y_Q IS
WHEN E=> z<= '1';
WHEN I => z <= '1';
WHEN OTHERS => z <= '0'; 
END CASE;

CASE Y_Q IS
WHEN A=> PS<=  "000000001";
WHEN B => PS<= "000000010";
WHEN C => PS<= "000000100";
WHEN D => PS<= "000001000";
WHEN E => PS<= "000010000";
WHEN F => PS<= "000100000";
WHEN G => PS<= "001000000";
WHEN H => PS<= "010000000";
WHEN I => PS<= "100000000";
END CASE;

END PROCESS Output_decode;

LEDG(0)<=z; --output assignment
LEDR<=PS;
END BEHAVIOR;