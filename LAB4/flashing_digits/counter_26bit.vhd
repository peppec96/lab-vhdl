library ieee;
USE ieee.std_logic_1164.all;

ENTITY counter_26bit IS 
PORT (CLK,EN,RESET: IN STD_LOGIC;
  COUT: OUT STD_LOGIC_VECTOR(0 TO 25);
  Q: OUT STD_LOGIC
);
END counter_26bit;

ARCHITECTURE Behavior OF counter_26bit IS 
SIGNAL T_INT: STD_LOGIC_VECTOR(0 TO 24);

COMPONENT Tff_with_and_port IS
PORT (T, CLK, Clear: IN STD_LOGIC;
     Q: BUFFER STD_LOGIC;
	  EN_OUT: OUT STD_LOGIC);
END COMPONENT;

COMPONENT T_flipflop IS
PORT(T, CLK, Clear: IN STD_LOGIC;
     Q: OUT STD_LOGIC);
END COMPONENT;

BEGIN 
--COUT<="11111111111111111111111111";
tff0: Tff_with_and_port PORT MAP
(EN,CLK,RESET,COUT(0),T_INT(0));

gen_tff: for i in 1 to 24 GENERATE
tffi: Tff_with_and_port PORT MAP
(T_INT(i-1),CLK,RESET,COUT(i),T_INT(i));
end GENERATE;

tff25: T_flipflop PORT MAP
(T_INT(24),CLK,RESET,COUT(25));

end Behavior;