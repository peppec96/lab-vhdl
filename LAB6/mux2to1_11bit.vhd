LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY mux2to1_11bit IS
 PORT(U,V: IN SIGNED (10 DOWNTO 0);
       S: IN STD_LOGIC;
       M: OUT SIGNED(10 DOWNTO 0));
end mux2to1_11bit;

ARCHITECTURE structure of mux2to1_11bit IS

BEGIN
    WITH S SELECT
   m<=U when '0',
      V when others;
end structure;