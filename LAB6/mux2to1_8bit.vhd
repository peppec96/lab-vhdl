LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY mux2to1_8bit IS
 PORT(U,V: IN SIGNED (7 DOWNTO 0);
       S: IN STD_LOGIC;
       M: OUT SIGNED(7 DOWNTO 0));
end mux2to1_8bit;

ARCHITECTURE structure of mux2to1_8bit IS

BEGIN
    with S select
   M<= U when '0',
       V when '1';
   

end structure;