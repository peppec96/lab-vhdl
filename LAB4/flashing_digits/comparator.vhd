library ieee;
USE ieee.std_logic_1164.all;

ENTITY comparator IS
PORT (X: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      y: OUT STD_LOGIC);
end comparator;

ARCHITECTURE behavior OF comparator IS
BEGIN 

y<=(X(3) AND X(2))OR(X(3) AND X(1));

end behavior;