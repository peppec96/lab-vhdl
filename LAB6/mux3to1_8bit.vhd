LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY mux3to1_8bit IS
 PORT(U,V,W: IN SIGNED (7 DOWNTO 0);
       S: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
       M: OUT SIGNED(7 DOWNTO 0));
end mux3to1_8bit;

ARCHITECTURE structure of mux3to1_8bit IS

BEGIN
  WITH S SELECT
      m<=U when "00",
         V when "01",
         W when "10";
end structure;