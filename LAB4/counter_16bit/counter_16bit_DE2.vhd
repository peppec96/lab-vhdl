library ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY counter_16bit_DE2 IS
PORT(SW: IN STD_LOGIC_VECTOR (1 DOWNTO 0);
    KEY: IN STD_LOGIC_VECTOR (3 DOWNTO 0);
     HEX3,HEX2,HEX1,HEX0: OUT STD_LOGIC_VECTOR(0 TO 6));
END counter_16bit_DE2;

ARCHITECTURE Behavior OF counter_16bit_DE2 IS
SIGNAL Q_INT: STD_LOGIC_VECTOR (0 TO 15);
COMPONENT counter_16bit IS
PORT(EN, CLOCK, Clear: IN STD_LOGIC;
     Q: OUT STD_LOGIC;
	  QOUT: BUFFER STD_LOGIC_VECTOR (0 TO 15));
END COMPONENT;

COMPONENT hexadecimal_converter IS
PORT( X: IN UNSIGNED(3 DOWNTO 0);
      D: BUFFER STD_LOGIC_VECTOR(0 TO 6));
end COMPONENT;

BEGIN
cnt: counter_16bit PORT MAP
(CLOCK=>KEY(0),EN=>SW(1),Clear=>SW(0),QOUT=>Q_INT);

conv1: hexadecimal_converter PORT MAP
(X(3)=>Q_INT(3),X(2)=>Q_INT(2),X(1)=>Q_INT(1),X(0)=>Q_INT(0),D=>HEX0);
conv2: hexadecimal_converter PORT MAP
(X(3)=>Q_INT(7),X(2)=>Q_INT(6),X(1)=>Q_INT(5),X(0)=>Q_INT(4),D=>HEX1);
conv3: hexadecimal_converter PORT MAP
(X(3)=>Q_INT(11),X(2)=>Q_INT(10),X(1)=>Q_INT(9),X(0)=>Q_INT(8),D=>HEX2);
conv4: hexadecimal_converter PORT MAP
(X(3)=>Q_INT(15),X(2)=>Q_INT(14),X(1)=>Q_INT(13),X(0)=>Q_INT(12),D=>HEX3);
END Behavior;

