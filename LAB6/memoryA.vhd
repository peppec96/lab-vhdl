library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity memoryA is
  port(CLK: in std_logic;
   DATA_IN:in signed(7 downto 0);
   ADDRESS:in unsigned(9 downto 0); --pointer for the cell
   CS,WR,RD: in std_logic;
      DATA_OUT:out signed(7 downto 0)
       );
end memoryA;


architecture Be of memoryA is
  subtype word is signed(7 downto 0);
  type mem is array(0 to 1023) of word;

  signal memory:mem:=(others=>"00000000");
 SIGNAL CONTROL: STD_LOGIC_VECTOR(2 DOWNTO 0); 

begin
 CONTROL<=CS& WR& RD ;
  main_p:process(CLK) is
  begin
  if rising_edge(CLK) then
    if (CONTROL = "101") then
      DATA_OUT<=memory(to_integer(ADDRESS));
    end if;

    if (CONTROL="110") then
      memory(to_integer(ADDRESS))<=DATA_IN;
    end if;
  end if;
  end process main_p;
  
end be;
