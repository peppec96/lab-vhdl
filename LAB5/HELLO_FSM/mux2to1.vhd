LIBRARY ieee;
USE ieee.std_logic_1164.all;


ENTITY mux2to1 IS
  PORT (x,y,s: IN STD_LOGIC;
       m: OUT STD_LOGIC);
end mux2to1;

ARCHITECTURE behavior of mux2to1 IS
BEGIN 
WITH s SELECT
m <= x when '0',
     y when OTHERS;
	  
end behavior;

 