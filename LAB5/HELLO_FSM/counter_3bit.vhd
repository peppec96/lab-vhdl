library ieee;
USE ieee.std_logic_1164.all;

ENTITY counter_3bit IS 
PORT (CLK,EN,RESET: IN STD_LOGIC;
  COUT: OUT STD_LOGIC_VECTOR(0 TO 2);
  Q: OUT STD_LOGIC
);
END counter_3bit;

ARCHITECTURE Behavior OF counter_3bit IS 
SIGNAL T_INT: STD_LOGIC_VECTOR(0 TO 2);

COMPONENT Tff_with_and_port IS
PORT (T, CLK, Clear: IN STD_LOGIC;
     Q: BUFFER STD_LOGIC;
	  EN_OUT: OUT STD_LOGIC);
END COMPONENT;

COMPONENT T_flipflop IS
PORT(T, CLK, Clear: IN STD_LOGIC;
     Q: OUT STD_LOGIC);
END COMPONENT;

BEGIN 
tff0: Tff_with_and_port PORT MAP
(EN,CLK,RESET,COUT(0),T_INT(0));

tff1: Tff_with_and_port PORT MAP
(T_INT(0),CLK,RESET,COUT(1),T_INT(1));

tff2: T_flipflop PORT MAP
(T_INT(1),CLK,RESET,COUT(2));

end Behavior;