library ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY counter_16bit_2 IS
PORT ( 
CLK,EN,RESET: IN STD_LOGIC;
Q: BUFFER UNSIGNED(15 DOWNTO 0));
end counter_16bit_2;

ARCHITECTURE Behavior OF counter_16bit_2 IS

BEGIN 
PROCESS(CLK,RESET)
BEGIN 
IF RESET = '0' THEN
Q<=(OTHERS=>'0');
ELSIF(CLK'EVENT AND CLK='1') THEN
IF EN='1' THEN
Q<= Q + 1;
END IF;
END IF;
END PROCESS;

END Behavior;