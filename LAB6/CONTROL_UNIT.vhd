library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY CONTROL_UNIT IS
PORT (START:IN STD_LOGIC;
      DONE: OUT STD_LOGIC;
		RESET: IN STD_LOGIC;
    CNT_RESET: OUT STD_LOGIC;
	 C11_RST: OUT STD_LOGIC;
    L1,L2,L3: OUT STD_LOGIC;
    CLOCK: IN STD_LOGIC;
    SEL: OUT STD_LOGIC_VECTOR(1 DOWNTO 0); 
	 SEL2: OUT STD_LOGIC;
	 CSA,CSB,WRA,WRB,RDA: OUT STD_LOGIC); --PER RST DELLA SOMMA
END CONTROL_UNIT;

ARCHITECTURE Behavior of CONTROL_UNIT IS

BEGIN 

---type 
END BEHAVIOR;