library ieee;
USE ieee.std_logic_1164.all;

ENTITY counter_26bit_wLOAD IS 
PORT (CLK,EN,CLEAR,LOAD: IN STD_LOGIC;
  COUT: BUFFER STD_LOGIC_VECTOR(0 TO 25);
  Q: OUT STD_LOGIC
);
END counter_26bit_wLOAD;

ARCHITECTURE Behavior OF counter_26bit_wLOAD IS 
SIGNAL EN_INT: STD_LOGIC_VECTOR(25 DOWNTO 1);
SIGNAL X_IN: STD_LOGIC_VECTOR(25 DOWNTO 0);
SIGNAL D_IN: STD_LOGIC_VECTOR(25 DOWNTO 0);

COMPONENT mux2to1
 PORT (x,y,s: IN STD_LOGIC;
       m: OUT STD_LOGIC);
END COMPONENT;

COMPONENT flipflop IS  
PORT (D, Clock, Resetn : IN  STD_LOGIC;   
Q   : OUT STD_LOGIC); 
END COMPONENT;


BEGIN 
X_IN(0)<=EN XOR COUT(0);

gen_x_in: for i IN 1 to 25 generate
X_IN(i)<=EN_INT(i) XOR COUT(i);
end generate;

Q<=EN_INT(25) AND COUT(25);

EN_INT(1)<=EN AND COUT(0);
gen_en: for i IN 1 to 24 generate
EN_INT(i+1)<=EN_INT(i) AND COUT(i);
end generate;

gen_mux: for i IN 0 to 25 generate
muxi:  mux2to1 PORT MAP
(X_IN(i),'0',LOAD,D_IN(i));
end generate;

gen_ff: for i IN 0 to 25 generate
ffi: flipflop PORT MAP
 (D_IN(i),CLK,Clear,COUT(i));
end generate;

end Behavior;