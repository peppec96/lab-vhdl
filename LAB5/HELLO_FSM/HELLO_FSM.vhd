LIBRARY ieee; 
USE ieee.std_logic_1164.all; 

ENTITY HELLO_FSM IS  
PORT ( KEY: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
       CLK_50: IN STD_LOGIC;
       HEX7,HEX6,HEX5,HEX4,HEX3,HEX2,HEX1,HEX0: OUT STD_LOGIC_VECTOR(0 TO 6)); 
END HELLO_FSM; 

ARCHITECTURE Behavior OF HELLO_FSM IS

SIGNAL LOAD: STD_LOGIC;  
SIGNAL C26_OUT: STD_LOGIC_VECTOR(0 TO 25);
SIGNAL EN_3C: STD_LOGIC;
SIGNAL AND2_OUT: STD_LOGIC;
SIGNAL C3_OUT: STD_LOGIC_VECTOR(0 TO 2);
SIGNAL CLK1: STD_LOGIC:='0';
------
TYPE State_type IS (A, B, C, D, E, F, G, H);  
SIGNAL y_Q, Y_D : State_type;  -- y_Q is present state, y_D is next state
SIGNAL REG8,REG7,REG6,REG5,REG4,REG3,REG2,REG1: STD_LOGIC_VECTOR(0 TO 6);
SIGNAL RESET: STD_LOGIC;
------
COMPONENT counter_26bit_wLOAD IS 
PORT (CLK,EN,CLEAR,LOAD: IN STD_LOGIC;
  COUT: BUFFER STD_LOGIC_VECTOR(0 TO 25);
  Q: OUT STD_LOGIC);
END COMPONENT;

COMPONENT counter_3bit IS 
PORT (CLK,EN,RESET: IN STD_LOGIC;
  COUT: OUT STD_LOGIC_VECTOR(0 TO 2);
  Q: OUT STD_LOGIC);
END COMPONENT;

COMPONENT regn IS  
GENERIC ( N : integer:=7);  
PORT (R   : IN  STD_LOGIC_VECTOR(N-1 DOWNTO 0);   
Clock: IN  STD_LOGIC;   
Q   : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0)); 
END COMPONENT;

BEGIN

C26: counter_26bit_wLOAD PORT MAP
(CLK_50,'1',KEY(0),LOAD,C26_OUT);

LOAD<=C26_OUT(0) AND C26_OUT(1) AND C26_OUT(2) AND C26_OUT(3) AND C26_OUT(4) AND 
C26_OUT(5) AND C26_OUT(6) AND C26_OUT(7) AND C26_OUT(8) AND C26_OUT(9) AND 
C26_OUT(10) AND C26_OUT(11) AND C26_OUT(12) AND C26_OUT(13) AND C26_OUT(14) AND 
C26_OUT(15) AND C26_OUT(16) AND C26_OUT(17) AND C26_OUT(18) AND C26_OUT(19) AND 
C26_OUT(20) AND C26_OUT(21) AND C26_OUT(22) AND C26_OUT(23) AND C26_OUT(24) AND C26_OUT(25);

EN_3C<=LOAD AND (NOT AND2_OUT);

C3: counter_3bit PORT MAP
(CLK_50,EN_3C,KEY(0),C3_OUT);

AND2_OUT<=C3_OUT(0) AND C3_OUT(1) AND C3_OUT(2);

CLK1<=LOAD AND AND2_OUT;

------

RESET<=KEY(0);

State_transition: PROCESS (y_Q,Y_D,CLK1) -- state table 
 BEGIN 
  CASE y_Q IS 
  
WHEN A=> IF (CLK1 = '0')  THEN Y_D <= A; 
ELSE Y_D <= B;  
 END IF; 

WHEN B=> IF (CLK1 = '0') THEN Y_D <= B;    
  ELSE Y_D <= C;    
 END IF;
 
WHEN C=> IF (CLK1 = '0')  THEN Y_D <= C;    
  ELSE Y_D <= D;    
 END IF;
 
WHEN D=> IF (CLK1 = '0')  THEN Y_D <= D;    
  ELSE Y_D <= E;    
 END IF;
 
WHEN E=> IF (CLK1 = '0')  THEN Y_D <= E;   
  ELSE Y_D <= F;    
 END IF;
 
WHEN F=> IF (CLK1 = '0') THEN Y_D <= F;    
  ELSE Y_D <= G;    
 END IF;
 
WHEN G=> IF (CLK1 = '0') THEN Y_D <= G;    
  ELSE Y_D <= H;    
 END IF;
 
WHEN H=> IF (CLK1 = '0') THEN Y_D <= H;    
  ELSE Y_D <= A;    
 END IF;
 
 END CASE; 
 END PROCESS State_transition; -- state table 
 
Register_process: PROCESS (CLK_50,RESET) -- state flip-flops 
BEGIN 
IF RESET = '0' THEN     
 Y_Q <= A;       
 ELSIF( rising_edge(CLK_50) ) THEN  
 Y_Q <= y_D;       
 END IF; 
END PROCESS Register_process;

Output_decode: PROCESS(REG8,REG7,REG6,REG5,REG4,REG3,REG2,REG1,Y_Q)
BEGIN

CASE Y_Q IS
WHEN A=> REG8<=  "1001000"; --H
         REG7<=  "0110000"; --E
         REG6<=  "1110000"; --L
			REG5<=  "1110000";   
			REG4<=  "0000001"; --O
			REG3<=  "1111111";
			REG2<=  "1111111";
			REG1<=  "1111111"; --EMPTY
       
WHEN B =>REG1<=  "1001000"; --H
         REG8<=  "0110000"; --E
         REG7<=  "1110000"; --L
			REG6<=  "1110000";   
			REG5<=  "0000001"; --O
			REG4<=  "1111111";
			REG3<=  "1111111";
			REG2<=  "1111111"; --EMPTY
			
WHEN C => REG2<=  "1001000"; --H
         REG1<=  "0110000"; --E
         REG8<=  "1110000"; --L
			REG7<=  "1110000";   
			REG6<=  "0000001"; --O
			REG5<=  "1111111";
			REG4<=  "1111111";
			REG3<=  "1111111"; --EMPTY
			
WHEN D => REG3<=  "1001000"; --H
         REG2<=  "0110000"; --E
         REG1<=  "1110000"; --L
			REG8<=  "1110000";   
			REG7<=  "0000001"; --O
			REG6<=  "1111111";
			REG5<=  "1111111";
			REG4<=  "1111111"; --EMPTY
			
WHEN E => REG4<=  "1001000"; --H
         REG3<=  "0110000"; --E
         REG2<=  "1110000"; --L
			REG1<=  "1110000";   
			REG8<=  "0000001"; --O
			REG7<=  "1111111";
			REG6<=  "1111111";
			REG5<=  "1111111"; --EMPTY
			
WHEN F => REG5<=  "1001000"; --H
         REG4<=  "0110000"; --E
         REG3<=  "1110000"; --L
			REG2<=  "1110000";   
			REG1<=  "0000001"; --O
			REG8<=  "1111111";
			REG7<=  "1111111";
			REG6<=  "1111111"; --EMPTY
			
WHEN G => REG6<=  "1001000"; --H
         REG5<=  "0110000"; --E
         REG4<=  "1110000"; --L
			REG3<=  "1110000";   
			REG2<=  "0000001"; --O
			REG1<=  "1111111";
			REG8<=  "1111111";
			REG7<=  "1111111"; --EMPTY
			
WHEN H => REG7<=  "1001000"; --H
         REG6<=  "0110000"; --E
         REG5<=  "1110000"; --L
			REG4<=  "1110000";   
			REG3<=  "0000001"; --O
			REG2<=  "1111111";
			REG1<=  "1111111";
			REG8<=  "1111111"; --EMPTY
END CASE;

END PROCESS Output_decode;

reg_8: regn PORT MAP
(REG8,CLK_50,HEX7);

reg_7: regn PORT MAP
(REG7,CLK_50,HEX6);

reg_6: regn PORT MAP
(REG6,CLK_50,HEX5);

reg_5: regn PORT MAP
(REG5,CLK_50,HEX4);

reg_4: regn PORT MAP
(REG4,CLK_50,HEX3);

reg_3: regn PORT MAP
(REG3,CLK_50,HEX2);

reg_2: regn PORT MAP
(REG2,CLK_50,HEX1);

reg_1: regn PORT MAP
(REG1,CLK_50,HEX0);

-----
END Behavior;

