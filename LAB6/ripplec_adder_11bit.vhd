LIBRARY ieee; 
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all; 

ENTITY ripple_carry_adder_11bit IS 
PORT ( Cin: STD_LOGIC;
        A,B: IN SIGNED (10 DOWNTO 0);
		  S: OUT SIGNED (10 DOWNTO 0);
		  Co: BUFFER STD_LOGIC;
		  Overflow: OUT STD_LOGIC);
END ripple_carry_adder_11bit;

ARCHITECTURE Behavior of ripple_carry_adder_11bit IS
SIGNAL COUT: STD_LOGIC_VECTOR(9 DOWNTO 0);

COMPONENT full_adder 
PORT ( Ci: STD_LOGIC;
        x,y: IN STD_LOGIC ;
		  s: OUT STD_LOGIC;
		  Co: OUT STD_LOGIC);
END COMPONENT;

BEGIN


fa0: full_adder PORT MAP
(x=>A(0),y=>B(0),Ci=>'0',s=>S(0),Co=>COUT(0));

fa1: full_adder PORT MAP
(x=>A(1),y=>B(1),Ci=>COUT(0),s=>S(1),Co=>COUT(1));

fa2: full_adder PORT MAP
(x=>A(2),y=>B(2),Ci=>COUT(1),s=>S(2),Co=>COUT(2));

fa3: full_adder PORT MAP
(x=>A(3),y=>B(3),Ci=>COUT(2),s=>S(3),Co=>COUT(3));

fa4: full_adder PORT MAP
(x=>A(4),y=>B(4),Ci=>COUT(3),s=>S(4),Co=>COUT(4));

fa5: full_adder PORT MAP
(x=>A(5),y=>B(5),Ci=>COUT(4),s=>S(5),Co=>COUT(5));

fa6: full_adder PORT MAP
(x=>A(6),y=>B(6),Ci=>COUT(5),s=>S(6),Co=>COUT(6));

fa7: full_adder PORT MAP
(x=>A(7),y=>B(7),Ci=>COUT(6),s=>S(7),Co=>COUT(7));

fa8: full_adder PORT MAP
(x=>A(8),y=>B(8),Ci=>COUT(7),s=>S(8),Co=>COUT(8));

fa9: full_adder PORT MAP
(x=>A(9),y=>B(9),Ci=>COUT(8),s=>S(9),Co=>COUT(9));

fa10: full_adder PORT MAP
(x=>A(10),y=>B(10),Ci=>COUT(9),s=>S(10),Co=>Co);

Overflow<=Co xor COUT(9);


END Behavior;




